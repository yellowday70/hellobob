import unittest
import app

ip = "13.209.15.210"
port = "8018"
url = f"{str(ip)}:{str(port)}"

class Test(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()
        
    def test_add(self):
        res = self.app.get(f"/add?a=1&b=2")
        assert b'3' in res.data

    def test_sub(self):
        res = self.app.get("/sub?a=5&b=2")
        assert b'3' in res.data

if __name__ == "__main__":
    unittest.main()
        
