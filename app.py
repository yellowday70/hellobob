from flask import Flask, request

app = Flask(__name__)

@app.route("/")
def hello():
    return "<h1>Hello World</h1>"

@app.route("/add")
def add():
    
    parameter_dict = request.args.to_dict()
    if len(parameter_dict) != 2:
        return 'num of parameter error'
    
    a = request.args.get("a", type=int)
    b = request.args.get("b", type=int)
    
    if a == None or b == None:
        return "Type error"

    return str(a+b)

@app.route("/sub")
def sub():
    
    parameter_dict = request.args.to_dict()
    if len(parameter_dict) != 2:
        return 'num of parameter error'
    
    a = request.args.get("a", type=int)
    b = request.args.get("b", type=int)
    
    if a == None or b == None:
        return "Type error"

    return str(a-b)


if __name__ == "__main__":
    app.run(host = "127.0.0.1", port="8080")
    
    